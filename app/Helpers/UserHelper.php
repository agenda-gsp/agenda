<?php

namespace App\Helpers;

use App\Models\User;

class UserHelper {


    public static function responseUserAuth($user) {
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        return json_encode(
            [
                'token' => $token, 
                'name' => $user->name, 
                'email' => $user->email, 
                'profile_image' => $user->profile_image, 
                'id' => $user->id,
                'user' => $user
            ]
        );
    }

}