<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\CompanyPlan;
use Illuminate\Http\Request;

use App\Models\User;
use App\Helpers\UserHelper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Socialite;

class ApiAuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->username)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $response = UserHelper::responseUserAuth($user);
                return response($response, 200);
            } else {
                if ($user->provider != null) {
                    $response = ["message" => "Usuário cadastrado com rede social"];
                    return response($response, 422);
                } else {
                    $response = ["message" => "Senha incorreta"];
                    return response($response, 422);
                }
            }
        } else {
            $response = ["message" => 'Usuário não existe'];
            return response($response, 422);
        }
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $company = Company::create([
            'name' => $request['company']
        ]);

        $companyPlan = CompanyPlan::create([
            'company_id' => $company->id,
            'max_order_count' => 30,
            'max_user_count' => 30,
            'max_people_count' => 30
        ]);

        $companyPlan->company_id = $company->id;
        $companyPlan->save();

        unset($request['company']);

        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->toArray());

        $user->company_id    = $company->id;
        $user->main_user     = 1;
        $user->save();

        $response = UserHelper::responseUserAuth($user);
        return response($response, 200);
    }
}
