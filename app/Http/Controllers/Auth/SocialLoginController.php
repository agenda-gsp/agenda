<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Cache;
use Illuminate\Support\Str;

use Validator;

use App\Models\Company;
use App\Models\CompanyPlan;
use App\Models\User;
use App\Helpers\UserHelper;

class SocialLoginController extends Controller
{


    public function __construct()
    {

    }


    public function SocialSignup(Request $r, $provider)
    {
        $validator = Validator::make($r->all(), [
            'code' => 'nullable|string',
            'hash' => 'nullable|string',
            'otp' => 'nullable|numeric',
            'token' => 'nullable|string',
            'secret' => 'nullable|string',

        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Incorrect Data Posted',
                'status' => 445,
            ];
        }

        $hash = $r->hash ?? null;
        $hashuser = Cache::get($hash);
        if ($hashuser) {
            return $this->SocialSignupNext($r, $hashuser);
        }
        try {

            // Socialite will pick response data automatic
            $user = Socialite::driver($provider)->stateless()->user();
            $token = $user->token ?? null;
            $refreshToken = $user->refreshToken ?? null;
            $expiresIn = $user->expiresIn ?? null;
            $tokenSecret = $user->tokenSecret ?? null;
            $id = $user->getId();
            $nickname = $user->getNickname();
            $name = $user->getName();
            $email = $user->getEmail();
            $profileImage = $user->getAvatar();

             $data =  [
                'name' => $name,
                'nickname' => $nickname,
                'profileImage' => $profileImage,
                'username' => '',
                'email' => $email,
                'provider' => $provider,
                'provider_id' => $id,
                'token' => $token,
                'tokenSecret' => $tokenSecret,
                'refreshToken' => $refreshToken,
                'expiresIn' => $expiresIn,

            ];
            // this is optional can be skip you can return your user data from here

            return $this->SocialSignupNext($r, $data);

        } catch (\Throwable $th) {
            return dd($th);

            logger($th);
        }

        return [
                'message' => 'Unknow Error',
                'status' => 445,
                'error' => $th
            ];
    }


    public function SocialSignupNext($request, $userdata)
    {
        $email = $userdata['email'];
        $provider = $userdata['provider'];
        $provider_id = $userdata['provider_id'];
        $name = $userdata['name'];
        $profileImage = $userdata['profileImage'];
        $usr = User::where('email', $email)->get();

        $user =  $usr->where('provider', $provider)
            ->where('provider_id', $provider_id)
            ->first();

        if ($user) {
            return $this->SocialLogin($request, $user);
        }
        $user = $usr->first();
        if ($user) {
            $user->update([

                'provider' => $provider,
                'provider_id' => $provider_id,

            ]);
            return $this->SocialLogin($request, $user);
        }

        $company = Company::create([
            'name' => 'Empresa de ' . $name
        ]);

        $companyPlan = CompanyPlan::create([
            'max_order_count' => 30,
            'max_user_count' => 30,
            'max_people_count' => 30
        ]);

        $companyPlan->company_id = $company->id;
        $companyPlan->save();

        $u =  User::create([
            'name' => $name,
            'email' => $email,
            'remember_token' => Str::random(10),
            'provider' => $provider,
            'provider_id' => $provider_id,
            'company_id' => $company['id'],
            'profile_image' => $profileImage
        ]);

        $u->company_id    = $company->id;
        $u->main_user     = 1;
        $u->save();

        // this is optional can be skip you can return your user data from here
        return $this->SocialLogin($request, $u);
    }



    public function SocialLogin($r, User $user)
    {
        $response = UserHelper::responseUserAuth($user);
        return response($response, 200);
    }


}