<?php

namespace App\Http\Controllers\Bank;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Bank;

class BankController extends Controller
{

    public function index() {

        $users = Bank::all();

        return $users->toJson();

    }

    public function show($id, Request $request) {

        $user = Bank::find($id);

        return $user->toJson();
        
    }

    public function store(Request $request) {
        $user = Bank::create($request->all());

        return $user->toJson();
    }

    public function update($id, Request $request) {
        
        Bank::findOrFail($id)->update($request->all());

        $user = Bank::findOrFail($id);

        return $user->toJson();

    }

    public function destroy($id, Request $request) {
        Bank::findOrFail($id)->delete();
    }
    
}
