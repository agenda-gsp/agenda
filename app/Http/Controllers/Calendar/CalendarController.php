<?php

namespace App\Http\Controllers\Calendar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderModel;
use App\Models\Service;
use App\Repositories\OrderModelRepository;

class CalendarController extends Controller
{
    public function index()
    {

        $models = OrderModel::has('order')
            ->has('service')
            ->with(['order', 'service'])
            ->where('order_model.starting', '!=', null)
            ->get();

        $modelsResult = [];

        foreach ($models as $model) {
            $modelsResult[] = $this->formatToCalendar($model);
        }


        $return = [
            'result' => $modelsResult,
            'count' => count($modelsResult)
        ];

        return json_encode($return);
    }


    public function update($id, Request $request)
    {
        $allowedFields = [
            'done'
        ];

        $newValues = [];

        foreach ($allowedFields as $field) {
            if ($request->has($field)) {
                $newValues[$field] = $request->input($field);
            }
        }

        $repository = new OrderModelRepository();
        $repository->update($id, $newValues);

        $model = OrderModel::with(['order', 'service'])->where('id', $id)->first();

        return json_encode([
            'message' => 'Alterado com sucesso!',
            'order_model' => $this->formatToCalendar($model)
        ]);
    }

    public function formatToCalendar($model)
    {
        $isLate = (!$model->done) && date('Y-m-d H:m:s', strtotime($model->ending)) < date('Y-m-d H:m:s');
        $color = $isLate ? '#f44336' : $model->model->agenda_color;

        if ($isLate) {
            $subject = '[ATRASADO] ' . $model->model->name;
        } else {
            $subject = $model->model->name;
        }

        return [
            'Id' => $model->id,
            'OrderId' => $model->order_id,
            'StatusId' => $model->done,
            'Subject' => $subject,
            'Description' => 'Pedido nº' . $model->order->id,
            'Color' => $color,
            'StartTime' => $model->starting,
            'EndTime' => $model->ending,
            'IsReadonly' => true,
            'ResourceId' => 1
        ];
    }
}
