<?php

namespace App\Http\Controllers\Comission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Comission;

class ComissionController extends Controller
{

    public function dashboard()
    {
        $sumNotPaid = Comission::where('paid', '!=', 1)->sum('value');
        $sumPaid = Comission::where('paid', '=', 1)->sum('value');

        $response = [
            'sumNotPaid' => $sumNotPaid,
            'sumPaid' => $sumPaid
        ];

        return json_encode($response);
    }

    public function index()
    {
        $users = Comission::with(['person', 'order_model.service'])->orderBy('created_at', 'desc')->get();

        return $users->toJson();
    }

    public function show($id, Request $request)
    {

        $user = Comission::with(['person', 'order_model.service', 'order_model.order'])->where('id', $id)->first();

        return $user->toJson();
    }

    public function store(Request $request)
    {
        $user = Comission::create($request->all());

        return $user->toJson();
    }

    public function update($id, Request $request)
    {

        Comission::findOrFail($id)->update($request->all());

        $user = Comission::findOrFail($id);

        return $user->toJson();
    }

    public function destroy($id, Request $request)
    {
        Comission::findOrFail($id)->delete();
    }
}
