<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Models\Company;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class CompanyController extends Controller
{

    public function index() {

        $users = Company::all();

        return $users->toJson();

    }

    public function getMyCompany(Request $request) {
        $companyIdLoggedUser = auth('api')->user()->company_id;

        return Company::find($companyIdLoggedUser)->toJson();
    }

    public function show($id, Request $request) {

        return Company::find($id)->toJson();
        
    }

    public function store(Request $request) {

        $company = Company::create($request->all());

        return $company->toJson();
    }

    public function update($id, Request $request) {
        
        Company::findOrFail($id)->update($request->all());

        $company = Company::findOrFail($id);

        return $company->toJson();

    }

    public function destroy($id, Request $request) {
        Company::findOrFail($id)->delete();
    }
    
}
