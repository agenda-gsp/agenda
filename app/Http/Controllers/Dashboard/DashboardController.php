<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\CompanyPlan;
use App\Models\Order;
use App\Models\OrderFiles;
use App\Models\OrderModel;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DashboardController extends Controller
{
    public function index(Request $request) {
        $now = (new \DateTime('now'))->format('Y-m-d H:i:s');
        $countOrders = Order::count();
        $countServices = OrderModel::where('model_type', Service::class)->count();

        $countServicesScheduled = OrderModel::where('model_type', Service::class)
                                            ->where('done', '!=', 1)
                                            ->where('starting', '>=', $now)
                                            ->count();

        $countServicesLate = OrderModel::where('model_type', Service::class)
                                            ->where('done', 0)
                                            ->where('starting', '<', $now)
                                            ->count();
        
        $countServicesDone = OrderModel::where('model_type', Service::class)
                                            ->where('done',1)
                                            ->count();

        $companyPlan = CompanyPlan::first();
        $maxOrders = $companyPlan->max_order_count ?? 1;
        $maxOrders = $maxOrders === 0 ? 1 : $maxOrders; // prenvet error

        $percLimitOrder = ($countOrders * 100) / $maxOrders;

        $info = [
            'countOrders' => $countOrders,
            'countServices' => $countServices,
            'countServicesScheduled' => $countServicesScheduled,
            'countServicesLate' => $countServicesLate,
            'countServicesDone' => $countServicesDone,
            'percLimitOrder' => $percLimitOrder
        ];

        return json_encode($info);
    }
}