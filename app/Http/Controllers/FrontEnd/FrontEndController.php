<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use App\Models\OrderFiles;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Socialite;
use Validator;

class FrontEndController extends Controller
{
    public function main() {
        return view('main');
    }

    public function callback(Request $r, $provider) {
        $validator = Validator::make($r->all(), [
            'code' => 'nullable|string',
            'hash' => 'nullable|string',
            'otp' => 'nullable|numeric',
            'token' => 'nullable|string',
            'secret' => 'nullable|string',

        ]);
        if ($validator->fails()) {
            return [
                'message' => 'Incorrect Data Posted',
                'status' => 445,
            ];
        }
        
        $user = Socialite::driver($provider)->stateless()->user();
        dd($user);
        return view('main');
    }

    public function file($module, $name) {

        $file = OrderFiles::where('filename', $name)->first();

        $pathToFile = storage_path() . $file->filepath;

        return Response::download($pathToFile);
    }
}