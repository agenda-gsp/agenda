<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Order;
use App\Models\OrderFiles;
use App\Models\OrderModel;
use App\Repositories\OrderModelRepository;
use Illuminate\Support\Facades\Storage;

class OrderController extends Controller
{

    public function upload($id, Request $request)
    {

        $order = Order::findOrFail($id);

        $idFiles = [];

        if ($request->exists('fileInfo')) {
            foreach ($request->input('fileInfo') as $object) {
                // Handle files that are already on the server
                $fileInfo = json_decode($object);
                if (isset($fileInfo->id)) {
                    $idFiles[] = $fileInfo->id;
                } else {
                    $fileDB = OrderFiles::where('order_id', $order->id)
                                        ->where('filename', $fileInfo->name)
                                        ->first();

                    $idFiles[] = $fileDB->id;
                }
            }
        }

        if ($request->exists('file')) {
            foreach ($request->file('file') as $file) {
                $filepath = Storage::disk('public')->put("orders/" . $file->hashName(), file_get_contents($file));
                $fileRow = $order->files()->create([
                    'name' => $file->getClientOriginalName(),
                    'filename' => $file->hashName(),
                    'extension' => $file->getClientOriginalExtension(),
                    'filepath' => Storage::disk('public')->url('orders/' . $file->hashName()),
                    'public_path' => public_path() . '/' . $file->hashName(),
                    'type' => $file->getMimeType()
                ]);
                $idFiles[] = $fileRow->id;
            }
        }

        OrderFiles::where('order_id', $order->id)->whereNotIn('id', $idFiles)->delete();


        return response()->json([
            'message' => 'success',
            'files' => $idFiles
        ]);
    }

    public function index()
    {

        $users = Order::with('client')->get();

        return $users->toJson();
    }

    public function show($id, Request $request)
    {

        $order = Order::with(['services', 'payments', 'files', 'client', 'order_items.comissions.person', 'order_items.service', 'order_items.personExecution'])->find($id);

        $orderArray = $order->toArray();

        foreach ($orderArray['services'] as $key => $service) {
            $service['pivot']['pivot_id'] = $service['pivot']['id'];
            unset($service['pivot']['id']);
            $orderArray['services'][$key] = array_merge($service, $service['pivot']);
        }

        foreach ($orderArray['payments'] as $key => $payment) {
            $orderArray['payments'][$key] = array_merge($payment, $payment['pivot']);
        }

        return json_encode($orderArray);
    }

    public function store(Request $request)
    {

        $order = Order::create($request->all());


        $order->services()->detach();
        foreach ($request->input('services') as $service) {
            $order->services()->attach(
                $service['id'],
                [
                    'starting' => $service['starting'] ?: null,
                    'ending' => $service['ending'] ?: null,
                    'value' => $service['value'] ?: null,
                    'done' => $service['done'] ?: null,
                    'person_comission_id' => isset($service['person_comission_id'])
                        ? $service['person_comission_id']
                        : 0
                ]
            );
        }

        $order->payments()->detach();
        foreach ($request->input('payments') as $payment) {
            $order->payments()->attach(
                $payment['id'],
                [
                    'value' => $payment['value'] ?: 0,
                    'date' => $payment['date'] ?: null,
                    'status_id' => $payment['status_id'] ?: 0
                ]
            );
        }

        return $order->toJson();
    }

    public function update($id, Request $request)
    {

        Order::findOrFail($id)->update($request->all());

        $order = Order::findOrFail($id);

        $repository = new OrderModelRepository();


        $idsUpdated = [];

        foreach ($request->input('services') as $service) {
            if (isset($service['pivot_id'])) {
                $repository->update(
                    $service['pivot_id'],
                    [
                        'starting' => $service['starting'] ?: null,
                        'ending' => $service['ending'] ?: null,
                        'value' => $service['value'] ?: null,
                        'done' => $service['done'] ?: null,
                        'person_comission_id' => isset($service['person_comission_id'])
                            ? $service['person_comission_id']
                            : 0
                    ]
                );

                $idsUpdated[] = $service['pivot_id'];
            }
        }

        OrderModel::where('order_id', $id)
            ->whereNotIn('id', $idsUpdated)
            ->delete();

        foreach ($request->input('services') as $service) {
            if (!isset($service['pivot_id'])) {
                $repository->create(
                    [
                        'order_id' => $id,
                        'model_id' => $service['id']
                    ],
                    [
                        'starting' => $service['starting'] ?: null,
                        'ending' => $service['ending'] ?: null,
                        'value' => $service['value'] ?: null,
                        'done' => $service['done'] ?: null,
                        'person_comission_id' => isset($service['person_comission_id'])
                            ? $service['person_comission_id']
                            : 0
                    ]
                );
            }
        }


        $order->payments()->detach();
        foreach ($request->input('payments') as $payment) {
            $order->payments()->attach(
                $payment['id'],
                [
                    'value' => $payment['value'] ?: 0,
                    'date' => $payment['date'] ?: null,
                    'status_id' => $payment['status_id'] ?: 0
                ]
            );
        }

        return $order->toJson();
    }

    public function destroy($id, Request $request)
    {
        Order::findOrFail($id)->delete();
    }
}
