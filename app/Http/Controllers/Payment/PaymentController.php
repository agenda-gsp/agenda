<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Models\OrderPayment;
use Illuminate\Http\Request;

use App\Models\Payment;

class PaymentController extends Controller
{
    public function dashboard(Request $request)
    {

        if($request->input('filters')) {

        }

        $countNotPaid = OrderPayment::query()->where('status_id', '=', 0)->count();
        $sumNotPaid = OrderPayment::query()->where('status_id', '=', 0)->sum('value');

        $countPaid = OrderPayment::query()->where('status_id', '=', 1)->count();
        $sumPaid = OrderPayment::query()->where('status_id', 1)->sum('value');

        $countLate = OrderPayment::query()
                        ->where('date', '<', date('Y-m-d'))
                        ->where('status_id', '=', 0)
                        ->count();

        $sumLate = OrderPayment::query()
                        ->where('date', '<', date('Y-m-d'))
                        ->where('status_id', '=', 0)
                        ->sum('value');

        $sumToReceive = OrderPayment::query()
                        ->where('date', '>=', date('Y-m-d'))
                        ->where('status_id', '=', 0)
                        ->sum('value');


        $response = [
            'countNotPaid' => $countNotPaid,
            'sumNotPaid' => $sumNotPaid,
            'countPaid' => $countPaid,
            'sumPaid' => $sumPaid,
            'countLate' => $countLate,
            'sumLate' => $sumLate,
            'sumToReceive' => $sumToReceive,

            'items' => OrderPayment::with(['payment', 'order.client'])->get()->toArray()
        ];

        return json_encode($response);
    }

    public function updatePayment($id, Request $request) {
        OrderPayment::findOrFail($id)->update($request->all());

        $payment = OrderPayment::findOrFail($id);

        return json_encode([ 'message' => 'Alterado com sucesso!', 'payment' => $payment ]);
    }

    public function index()
    {

        $users = Payment::with(['bank'])->get();

        return $users->toJson();
    }

    public function show($id, Request $request)
    {

        $user = Payment::find($id);

        return $user->toJson();
    }

    public function store(Request $request)
    {
        $user = Payment::create($request->all());

        return $user->toJson();
    }

    public function update($id, Request $request)
    {

        Payment::findOrFail($id)->update($request->all());

        $user = Payment::findOrFail($id);

        return $user->toJson();
    }

    public function destroy($id, Request $request)
    {
        Payment::findOrFail($id)->delete();
    }
}
