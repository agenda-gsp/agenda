<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ServiceCategory;

class ServiceCategoryController extends Controller
{

    public function index() {

        $users = ServiceCategory::all();

        return $users->toJson();

    }

    public function show($id, Request $request) {

        $user = ServiceCategory::find($id);

        return $user->toJson();
        
    }

    public function store(Request $request) {
        $user = ServiceCategory::create($request->all());

        return $user->toJson();
    }

    public function update($id, Request $request) {
        
        ServiceCategory::findOrFail($id)->update($request->all());

        $user = ServiceCategory::findOrFail($id);

        return $user->toJson();

    }

    public function destroy($id, Request $request) {
        ServiceCategory::findOrFail($id)->delete();
    }
    
}
