<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Service;

class ServiceController extends Controller
{

    public function index() {

        $users = Service::with('category')->get();

        return $users->toJson();

    }

    public function show($id, Request $request) {

        $user = Service::find($id);

        return $user->toJson();
        
    }

    public function store(Request $request) {
        $user = Service::create($request->all());

        return $user->toJson();
    }

    public function update($id, Request $request) {
        
        Service::findOrFail($id)->update($request->all());

        $user = Service::findOrFail($id);

        return $user->toJson();

    }

    public function destroy($id, Request $request) {
        Service::findOrFail($id)->delete();
    }
    
}
