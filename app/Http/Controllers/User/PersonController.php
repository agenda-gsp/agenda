<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Person;

class PersonController extends Controller
{

    public function index() {

        $users = Person::all();

        return $users->toJson();

    }

    public function show($id, Request $request) {

        $user = Person::with([ 'orders', 'orders.files', 'orders.order_payments.payment', 'comissions' ])->find($id);

        return $user->toJson();
        
    }

    public function store(Request $request) {
        $user = Person::create($request->all());

        foreach($request->input('phones') as $phone) {
            $user->phones()->create($phone);
        }

        return $user->toJson();
    }

    public function update($id, Request $request) {
        
        Person::findOrFail($id)->update($request->all());

        $user = Person::findOrFail($id);

        $user->phones()->delete();

        foreach($request->input('phones') as $phone) {
            $user->phones()->create($phone);
        }

        return $user->toJson();

    }

    public function destroy($id, Request $request) {
        Person::findOrFail($id)->delete();
    }
    
}
