<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserAssociated;

class UserController extends Controller
{

    public function index()
    {

        $users = UserAssociated::with('person')->get();

        return $users->toJson();
    }

    public function show($id, Request $request)
    {

        $user = UserAssociated::find($id);

        return $user->toJson();
    }

    public function store(Request $request)
    {
        $params = $request->all();

        if (
            isset($params['password'])
            && isset($params['confirm_password'])
            && $params['password'] == $params['confirm_password']
        ) {
            $params['password'] = bcrypt($params['password']);
        } else {
            unset($params['password']);
            unset($params['confirm_password']);
        }

        $user = UserAssociated::create($params);

        return $user->toJson();
    }

    public function update($id, Request $request)
    {

        $params = $request->all();

        if (
            isset($params['password'])
            && isset($params['confirm_password'])
            && $params['password'] == $params['confirm_password']
        ) {
            $params['password'] = bcrypt($params['password']);
        } else {
            unset($params['password']);
            unset($params['confirm_password']);
        }

        UserAssociated::findOrFail($id)->update($params);

        $user = UserAssociated::findOrFail($id);

        return $user->toJson();
    }

    public function destroy($id, Request $request)
    {
        UserAssociated::findOrFail($id)->delete();
    }
}
