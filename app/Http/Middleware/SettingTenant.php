<?php

namespace App\Http\Middleware;

use Closure;
use Landlord;
use Illuminate\Support\Facades\Auth;

class SettingTenant
{

    public function handle($request, Closure $next)
    {
        $tenantId = auth('api')->user()->company_id;

        Landlord::addTenant('company_id', $tenantId );

        return $next($request);

    }
}