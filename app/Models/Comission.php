<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Models\Order;
use App\Models\Service;
use Illuminate\Database\Eloquent\Model;

class Comission extends Model
{
    protected $table = 'comissions';

    protected $fillable = [
        'order_model_id',
        'person_id',
        'payment_id',
        'value',
        'type',
        'paid'
    ];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }

    public function order_model()
    {
        return $this->belongsTo(OrderModel::class);
    }
}