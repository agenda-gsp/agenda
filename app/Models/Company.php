<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

use Torzer\Awesome\Landlord\BelongsToTenants;

class Company extends Model
{
    use SoftDeletes, HasApiTokens, BelongsToTenants;

    public static function boot() {
        parent::boot();

        self::created(function($model){
            $model->company_id = $model->id;
            $model->save();
        });
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'short_name',
        'CNPJ',
        'address',
        'logo'
    ];

}
