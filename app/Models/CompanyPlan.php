<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

use Torzer\Awesome\Landlord\BelongsToTenants;

class CompanyPlan extends Model
{
    use HasApiTokens, BelongsToTenants;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'max_order_count',
        'max_user_count',
        'max_people_count'
    ];

}
