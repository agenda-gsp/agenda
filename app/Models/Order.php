<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\Traits\HasCustomId;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Torzer\Awesome\Landlord\BelongsToTenants;

class Order extends Model
{
    use HasApiTokens, BelongsToTenants, HasCustomId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'person_id',
        'employee_id',
        'seller_id',
        'type', // 0 to  orçamento, 1 to order
        'discount',
        'real_cost',
        'total_cost',
        'description',
        'starting',
        'ending'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'starting' => 'datetime',
        'ending' => 'datetime',
    ];

    protected $appends = [
        'status'
    ];


    public function getStatusAttribute()
    {
        if ($this->services === null) {
            return 'Sem Itens';
        }

        $total_order = 0;
        $sumDone = 0;
        $countServices = 0;

        foreach ($this->services as $service) {
            if ($service->pivot->starting == null) {
                if ($service->pivot->done != 1) {
                    return 'Agendamento Pendente';
                }
            }

            $sumDone += $service->pivot->done;
            $countServices++;

            $total_order += floatval($service->pivot->value);
        }

        $allServicesDone = $sumDone == $countServices;


        if ($this->payments === null) {
            return 'Sem Pagamento';
        }

        $total_paid = 0;

        foreach ($this->payments as $payment) {
            if ($payment->pivot->status_id) {
                $total_paid += floatval($payment->pivot->value);
            }
        }

        $realOrderValue = $total_order - floatval($this->attributes['discount']);

        if ($total_paid >= $realOrderValue) {
            if ($allServicesDone) {
                return 'Finalizado';
            } else {
                return 'Pago – Serviços Pendentes';
            }
        } else {
            return 'Pagamento Pendente';
        }
    }


    public function client()
    {
        return $this->belongsTo('App\Models\Person', 'person_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Person', 'employee_id', 'id');
    }

    public function seller()
    {
        return $this->belongsTo('App\Models\Person', 'seller_id', 'id');
    }

    public function services()
    {
        return $this->morphedByMany('App\Models\Service', 'model', 'order_model')
            ->using(OrderModel::class)
            ->withPivot(
                'starting',
                'ending',
                'value',
                'done',
                'person_comission_id',
                'id'
            );
    }

    public function order_items()
    {
        return $this->hasMany(OrderModel::class, 'order_id');
    }

    /**
     * The payments that belong to the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function payments(): BelongsToMany
    {
        return $this->belongsToMany(Payment::class, 'order_payment', 'order_id', 'payment_id')
            ->using(OrderPayment::class)
            ->withPivot('value', 'date', 'status_id')
            ->withTimestamps();
    }

    /**
     * The payments that belong to the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function order_payments(): HasMany
    {
        return $this->hasMany(OrderPayment::class, 'order_id', 'id');
    }

    /**
     * Get all of the files for the Order
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files(): HasMany
    {
        return $this->hasMany(OrderFiles::class, 'order_id', 'id');
    }
}
