<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Torzer\Awesome\Landlord\BelongsToTenants;

class OrderFiles extends Model
{
    use HasApiTokens, BelongsToTenants;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'name',
        'filename',
        'filepath',
        'public_path',
        'extension',
        'type'
    ];
}
