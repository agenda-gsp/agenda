<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\MorphPivot;
use App\Models\Order;
use App\Models\Service;
use Torzer\Awesome\Landlord\BelongsToTenants;


class OrderModel extends MorphPivot
{
    use BelongsToTenants;

    protected $table = 'order_model';

    protected $casts = [
        'starting' => 'datetime',
        'ending' => 'datetime',
    ];

    protected $fillable = [
        'starting',
        'ending',
        'value',
        'person_comission_id',
        'done',
    ];

    public function service()
    {
        return $this->morphTo('model');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function personExecution()
    {
        return $this->belongsTo(Person::class, 'person_comission_id');
    }

    public function comissions()
    {
        return $this->hasMany(Comission::class, 'order_model_id');
    }
}