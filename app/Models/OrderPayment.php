<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Torzer\Awesome\Landlord\BelongsToTenants;

class OrderPayment extends Pivot
{
    use BelongsToTenants;
    
    protected $casts = [
        'date' => 'datetime',
        'status_id' =>'boolean'
    ];

    protected $fillable = [
        'value', 'date', 'status_id'
    ];

    protected $appends = [
        'status_overview'
    ];

    public function getStatusOverviewAttribute() {
        if(!$this->attributes['status_id']) {
            if($this->attributes['date'] < date('Y-m-d')) {
                return 2; // It's late
            }

            return 1; // Not paid
        }

        return 0;
    }

    /**
     * Get the order that owns the OrderPayment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order(): BelongsTo
    {
        return $this->belongsTo(Order::class, 'order_id', 'id');
    }

    /**
     * Get the order that payment the OrderPayment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment(): BelongsTo
    {
        return $this->belongsTo(Payment::class, 'payment_id', 'id');
    }
}
