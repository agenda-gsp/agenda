<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use Torzer\Awesome\Landlord\BelongsToTenants;

class Payment extends Model
{
    use HasApiTokens, BelongsToTenants, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'bank_id'
    ];
    

    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }

}
