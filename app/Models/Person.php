<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\Traits\HasCustomId;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Torzer\Awesome\Landlord\BelongsToTenants;

class Person extends Model
{
    use HasApiTokens, BelongsToTenants, HasCustomId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'address',
        'neighborhood',
        'city',
        'description'
    ];

    protected $appends = [ 
        'main_phone' 
    ];


    public function phones()
    {
        return $this->hasMany('App\Models\PersonPhone', 'person_id', 'id');
    }

    public function getMainPhoneAttribute() 
    {
        return 
            $this->phones->isEmpty() 
            ? 'Não cadastrado'
            : $this->phones->first()->phone;
    }

    /**
     * Get all of the orders for the Person
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(): HasMany
    {
        return $this->hasMany(Order::class, 'person_id', 'id');
    }

    /**
     * Get all of the comissions for the Person
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comissions(): HasMany
    {
        return $this->hasMany(Comission::class, 'person_id', 'id');
    }



}
