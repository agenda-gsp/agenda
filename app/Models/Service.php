<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\Traits\HasCustomId;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Torzer\Awesome\Landlord\BelongsToTenants;

class Service extends Model
{
    use HasApiTokens, BelongsToTenants, HasCustomId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'cost',
        'min_cost',
        'base_comission_sell',
        'base_comission_sell_type',
        'max_comission_sell',
        'max_comission_sell_type',
        'base_comission_exec',
        'base_comission_exec_type',
        'max_comission_exec',
        'max_comission_exec_type',
        'duration',
        'agenda_color',
        'service_category_id',
    ];

    public function orders() {
        return $this->morphToMany('App\Models\Order', 'model', 'order_model')
                    ->using(OrderModel::class)
                    ->withPivot(
                        'starting',
                        'ending',
                        'value',
                        'done',
                        'person_comission_id'
                    );
    }


    /**
     * Get the category that owns the Service
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(ServiceCategory::class, 'service_category_id', 'id');
    }



}
