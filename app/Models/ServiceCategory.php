<?php

namespace App\Models;

use App\Models\Traits\HasCustomId;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Torzer\Awesome\Landlord\BelongsToTenants;

class ServiceCategory extends Model
{
    use HasApiTokens, BelongsToTenants, HasCustomId;

    protected $table = 'service_categories';

    protected $fillable = [
        'name',
        'description'
    ];
}
