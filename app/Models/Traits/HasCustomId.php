<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Model;


trait HasCustomId { 

    protected static $customIdField = 'custom_id';

    public static function bootHasCustomId()
    {
        static::creating(function (Model $model) {

            $maxId = $model->max(static::$customIdField);

            $model->{static::$customIdField} = ($maxId + 1) ?? 1;

        });
    }

}