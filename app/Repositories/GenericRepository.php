<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

class GenericRepository 
{
	private $model;

	public function __construct(Model $model)
	{
		$this->model = $model;
	}
}