<?php

namespace App\Repositories;

use App\Models\OrderModel;
use App\Models\Comission;
use App\Models\Order;

class OrderModelRepository
{
    private $model;

    public function create(array $keys, array $attributes)
    {
        $order = Order::findOrFail($keys['order_id']);

        $order->services()->attach(
            $keys['model_id'],
            $attributes
        );

        if (isset($attributes['done'])) {
            $orderModel = OrderModel::latest()->first();

            $this->changeStatus($orderModel->id, $attributes['done']);
        }
    }

    public function update($id, array $attributes)
    {
        $model = OrderModel::findOrFail($id);

        if (
            isset($attributes['done'])
            && floatval($model->done) != floatval($attributes['done'])
        ) {
            $this->changeStatus($id, boolval($attributes['done']));
        }

        OrderModel::findOrFail($id)->update($attributes);
    }

    public function changeStatus($id, $to = false)
    {
        if ($to) {
            // false to true
            $this->generateComission($id);
        } else {
            // true to false
            Comission::where('order_model_id', '=', $id)->delete();
        }
    }

    public function generateComission($id)
    {
        $model = OrderModel::with(['order', 'service'])->where('id', $id)->first();

        if ($model->model != null) {

            // Employee

            if ($model->model->base_comission_exec != null && $model->model->base_comission_exec != 0) {

                $type = $model->model->base_comission_exec_type ? $model->model->base_comission_exec_type : 'perc';

                $value = $model->model->base_comission_exec;

                if ($type == 'perc') {
                    $serviceValue = floatval($model->value);

                    $comissionValue = ($serviceValue * $value) / 100;
                } else {
                    $comissionValue = $value;
                }

                Comission::create([
                    'order_model_id' => $model->id,
                    'person_id' => $model->person_comission_id,
                    'type' => 'exec',
                    'value' => $comissionValue
                ]);
            }

            // Seller

            if ($model->model->base_comission_sell != null && $model->model->base_comission_sell != 0) {

                $type = $model->model->base_comission_sell_type ? $model->model->base_comission_sell_type : 'perc';

                $value = $model->model->base_comission_sell;

                if ($type == 'perc') {
                    $serviceValue = floatval($model->value);

                    $comissionValue = ($serviceValue * $value) / 100;
                } else {
                    $comissionValue = $value;
                }

                Comission::create([
                    'order_model_id' => $model->id,
                    'person_id' => $model->order->seller_id,
                    'type' => 'sell',
                    'value' => $comissionValue
                ]);
            }
        }
    }
}
