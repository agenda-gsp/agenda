<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors', 'json.response'], 'namespace'=>'App\Http\Controllers' ], function () {
    
    Route::post('/users/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/users/register','Auth\ApiAuthController@register')->name('register.api');

    Route::post('/sociallogin/{provider}', 'Auth\SocialLoginController@SocialSignup');
    Route::get('/auth/{provider}/callback', 'FrontEnd\FrontEndController@main')->where('provider', '.*');
    

});

Route::group(['middleware' => ['auth:api', 'tenant'], 'namespace'=>'App\Http\Controllers' ], function () {

    Route::get('calendar/', 'Calendar\CalendarController@index');
    Route::resource('calendar', 'Calendar\CalendarController');

    Route::resource('banks', 'Bank\BankController');
    
    Route::post('payments/dashboard/{id}', 'Payment\PaymentController@updatePayment');
    Route::post('payments/dashboard', 'Payment\PaymentController@dashboard');
    Route::resource('payments', 'Payment\PaymentController');


    Route::get('dashboard', 'Dashboard\DashboardController@index');
    Route::post('dashboard', 'Dashboard\DashboardController@index');


    Route::post('comissions/dashboard', 'Comission\ComissionController@dashboard');
    Route::resource('comissions', 'Comission\ComissionController');

    Route::resource('users', 'User\UserController');

    Route::resource('persons', 'User\PersonController');

    Route::post('orders/{id}/files', 'Order\OrderController@upload');
    Route::resource('orders', 'Order\OrderController');

    Route::resource('service_categories', 'Service\ServiceCategoryController');

    Route::resource('services', 'Service\ServiceController');

    Route::get('company/my', 'Company\CompanyController@getMyCompany');
    Route::resource('company', 'Company\CompanyController');

    // our routes to be protected will go in here
    Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
});
